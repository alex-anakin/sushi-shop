package ua.org.sushi.dao.model;

import lombok.Getter;
import lombok.Setter;
import ua.org.sushi.enums.ProductType;

@Getter
@Setter
public class ProductModel {

	private Long id;
	private String name;
	private Double price;
	private ProductType type;
	private String imageUrl;

	public ProductModel() {
	}

	public ProductModel(String name, Double price, ProductType type, String imageUrl) {
		this.name = name;
		this.price = price;
		this.type = type;
		this.imageUrl = imageUrl;
	}
}