package ua.org.sushi.dao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerModel {
	private Long id;
	private String email;
	private String name;
	private String address;
}