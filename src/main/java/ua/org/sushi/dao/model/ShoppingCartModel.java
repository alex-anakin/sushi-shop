package ua.org.sushi.dao.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ShoppingCartModel {

	private Long id;
	private List<ProductOrderModel> orders;
}