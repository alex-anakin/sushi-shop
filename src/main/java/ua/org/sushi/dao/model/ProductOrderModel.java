package ua.org.sushi.dao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductOrderModel {

	private Long id;
	private ProductModel product;
	private Integer quantity;
}