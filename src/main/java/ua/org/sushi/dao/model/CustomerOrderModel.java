package ua.org.sushi.dao.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerOrderModel {

	private Long id;
	private CustomerModel customer;
	private ShoppingCartModel shoppingCart;
}