package ua.org.sushi.dao.entity;

import lombok.Getter;
import lombok.Setter;
import ua.org.sushi.enums.ProductType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class Product {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@NotNull
	private String name;

	@NotNull
	private Double price;

	@NotNull
	private ProductType type;

	@NotNull
	private String imageUrl;
}