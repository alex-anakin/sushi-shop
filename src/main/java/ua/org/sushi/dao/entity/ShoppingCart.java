package ua.org.sushi.dao.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class ShoppingCart {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long id;

	@Column
	@ElementCollection(targetClass=ProductOrder.class)
	private List<ProductOrder> orders;
}