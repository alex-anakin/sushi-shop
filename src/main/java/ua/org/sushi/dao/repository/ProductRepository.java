package ua.org.sushi.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.org.sushi.dao.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
}