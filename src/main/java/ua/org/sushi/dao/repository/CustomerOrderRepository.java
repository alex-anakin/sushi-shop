package ua.org.sushi.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.org.sushi.dao.entity.CustomerOrder;

public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, Long> {
}