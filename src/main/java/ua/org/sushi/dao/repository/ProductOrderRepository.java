package ua.org.sushi.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.org.sushi.dao.entity.ProductOrder;

public interface ProductOrderRepository extends CrudRepository<ProductOrder, Long> {
}