package ua.org.sushi.dao.repository;

import org.springframework.data.repository.CrudRepository;
import ua.org.sushi.dao.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}