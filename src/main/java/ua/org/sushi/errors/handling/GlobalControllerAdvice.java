package ua.org.sushi.errors.handling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

@ControllerAdvice(basePackages = {"ua.org.sushi.controller"} )
public class GlobalControllerAdvice {

	@ResponseStatus(value= HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ConstraintViolationException.class, IllegalArgumentException.class})
	public @ResponseBody ExceptionJsonInfo resolveViolations(HttpServletRequest request, Exception exception) {
		ExceptionJsonInfo response = new ExceptionJsonInfo();
		response.setUrl(request.getRequestURL().toString());
		response.setMessage(exception.getMessage());
		return response;
	}
}
