package ua.org.sushi.errors.handling;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ExceptionJsonInfo {
	private String url;
	private String message;
}
