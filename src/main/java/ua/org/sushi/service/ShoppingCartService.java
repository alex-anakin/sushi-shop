package ua.org.sushi.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.org.sushi.dao.entity.ProductOrder;
import ua.org.sushi.dao.entity.ShoppingCart;
import ua.org.sushi.dao.model.ShoppingCartModel;
import ua.org.sushi.dao.repository.ProductOrderRepository;
import ua.org.sushi.dao.repository.ShoppingCartRepository;

@Service
public class ShoppingCartService {

	private final ShoppingCartRepository repository;
	private final ProductOrderRepository orderRepository;
	private final MapperFacade mapper;

	@Autowired
	public ShoppingCartService(ShoppingCartRepository repository, ProductOrderRepository orderRepository, MapperFacade mapper) {
		this.repository = repository;
		this.orderRepository = orderRepository;
		this.mapper = mapper;
	}

	public ShoppingCartModel saveCart(ShoppingCartModel model) {
		ShoppingCart cart = mapper.map(model, ShoppingCart.class);
		orderRepository.save(cart.getOrders());
		ShoppingCart savedCart = repository.save(cart);
		return mapper.map(savedCart, ShoppingCartModel.class);
	}

	public ShoppingCartModel getCart(Long id) {
		return mapper.map(repository.findOne(id), ShoppingCartModel.class);
	}
}