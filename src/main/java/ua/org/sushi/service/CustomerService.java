package ua.org.sushi.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.org.sushi.dao.entity.Customer;
import ua.org.sushi.dao.model.CustomerModel;
import ua.org.sushi.dao.repository.CustomerRepository;

import java.util.List;

@Service
public class CustomerService {

	private final CustomerRepository repository;
	private final MapperFacade mapper;

	@Autowired
	public CustomerService(CustomerRepository repository, MapperFacade mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	public CustomerModel saveCustomer(CustomerModel model) {
		Customer customer = new Customer();
		customer.setEmail(model.getEmail());
		customer.setName(model.getName());
		customer.setAddress(model.getAddress());

		return mapper.map(repository.save(customer), CustomerModel.class);
	}

	public CustomerModel getCustomer(final Long id) {
		return mapper.map(repository.findOne(id), CustomerModel.class);
	}

	public List<CustomerModel> getCustomers() {
		return mapper.mapAsList(repository.findAll(), CustomerModel.class);
	}
}