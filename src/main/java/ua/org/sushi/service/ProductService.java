package ua.org.sushi.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.org.sushi.dao.entity.Product;
import ua.org.sushi.dao.model.ProductModel;
import ua.org.sushi.dao.repository.ProductRepository;
import ua.org.sushi.enums.ProductType;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ProductService {

	private final ProductRepository repository;
	private final MapperFacade mapper;

	@Autowired
	public ProductService(ProductRepository repository, MapperFacade mapper) {
		this.repository = repository;
		this.mapper = mapper;
	}

	@PostConstruct
	public void init() {

		String addition = "http://i67.tinypic.com/2lmo19i.jpg";
		String filMaki = "http://i63.tinypic.com/25543rr.jpg";
		String amMaki = "http://i67.tinypic.com/15eg29w.jpg";
		String zolDrakon = "http://i65.tinypic.com/10r2vsl.jpg";
		String guniTataki = "http://i66.tinypic.com/10ga4n5.jpg";
		String misoSoup = "http://i67.tinypic.com/2myb40m.jpg";
		String lapsha = "http://i66.tinypic.com/2j6a074.jpg";
		String shrimps = "http://i66.tinypic.com/p3qd1.jpg";
		String rolls = "http://i68.tinypic.com/atxjys.jpg";

		saveProduct(new ProductModel("Филадельфия маки", 120.00d, ProductType.MAIN, filMaki));
		saveProduct(new ProductModel("Америка маки", 140.00d, ProductType.MAIN, amMaki));
		saveProduct(new ProductModel("Золотой дракон", 150.00d, ProductType.MAIN, zolDrakon));
		saveProduct(new ProductModel("Гюни татаки", 90.00d, ProductType.MAIN, guniTataki));
		saveProduct(new ProductModel("Мисо суп", 35.000d, ProductType.MAIN, misoSoup));
		saveProduct(new ProductModel("Лапша со свининой", 97.00d, ProductType.MAIN, lapsha));
		saveProduct(new ProductModel("Креветки темпура", 68.00d, ProductType.MAIN, shrimps));
		saveProduct(new ProductModel("Роллы с лососем", 54.00d, ProductType.MAIN, rolls));
		saveProduct(new ProductModel("Васаби", 5.00d, ProductType.ADDITION, addition));
		saveProduct(new ProductModel("Севый соус", 5.00d, ProductType.ADDITION, addition));
		saveProduct(new ProductModel("Сыр филадельфия", 10.00d, ProductType.ADDITION, addition));
		saveProduct(new ProductModel("Устричный соус", 8.00d, ProductType.ADDITION, addition));
		saveProduct(new ProductModel("Палочки", 0.00d, ProductType.ADDITION, addition));
	}

	public List<ProductModel> getProducts() {
		Iterable<Product> products = repository.findAll();
		return mapper.mapAsList(products, ProductModel.class);
	}

	public ProductModel saveProduct(ProductModel model) {
		Product product = mapper.map(model, Product.class);
		Product savedProduct = repository.save(product);
		return mapper.map(savedProduct, ProductModel.class);
	}

	public ProductModel getProduct(Long id) {
		return mapper.map(repository.findOne(id), ProductModel.class);
	}
}