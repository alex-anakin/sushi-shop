package ua.org.sushi.service;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.org.sushi.dao.entity.CustomerOrder;
import ua.org.sushi.dao.model.CustomerOrderModel;
import ua.org.sushi.dao.repository.CustomerOrderRepository;
import ua.org.sushi.dao.repository.CustomerRepository;

@Service
public class CustomerOrderService {

	private final CustomerOrderRepository customerOrderRepository;
	private final CustomerRepository customerRepository;
	private final MapperFacade mapper;

	@Autowired
	public CustomerOrderService(CustomerOrderRepository customerOrderRepository, CustomerRepository customerRepository,
	                            MapperFacade mapper) {
		this.customerOrderRepository = customerOrderRepository;
		this.customerRepository = customerRepository;
		this.mapper = mapper;
	}

	public CustomerOrderModel saveOrder(CustomerOrderModel orderModel) {
		validateOrder(orderModel);
		CustomerOrder order = mapper.map(orderModel, CustomerOrder.class);
		CustomerOrder savedOrder = customerOrderRepository.save(order);
		return mapper.map(savedOrder, CustomerOrderModel.class);
	}

	public CustomerOrderModel findOrder(long id) {
		CustomerOrder order = customerOrderRepository.findOne(id);
		return mapper.map(order, CustomerOrderModel.class);
	}

	private void validateOrder(CustomerOrderModel orderModel) {
		Long customerId = orderModel.getCustomer().getId();
		if (customerId == null || customerRepository.findOne(customerId) == null) {
			throw new IllegalArgumentException("Order with unknown customer cannot be saved");
		}
	}
}