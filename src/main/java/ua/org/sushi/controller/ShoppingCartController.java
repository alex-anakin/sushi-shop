package ua.org.sushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.org.sushi.dao.model.ShoppingCartModel;
import ua.org.sushi.service.ShoppingCartService;

@CrossOrigin
@RestController
public class ShoppingCartController {

	private final ShoppingCartService shoppingCartService;

	@Autowired
	public ShoppingCartController(ShoppingCartService shoppingCartService) {
		this.shoppingCartService = shoppingCartService;
	}

	@PostMapping(value = "api/carts")
	public ShoppingCartModel saveCart(@RequestBody ShoppingCartModel model) {
		return shoppingCartService.saveCart(model);
	}

	@GetMapping(value = "api/carts/{id}")
	public ShoppingCartModel getCart(@PathVariable Long id) {
		return shoppingCartService.getCart(id);
	}

	@PutMapping(value = "api/carts")
	public ShoppingCartModel updateCart(@RequestBody ShoppingCartModel model) {
		return shoppingCartService.saveCart(model);
	}
}