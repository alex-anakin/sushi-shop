package ua.org.sushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.org.sushi.dao.model.CustomerOrderModel;
import ua.org.sushi.service.CustomerOrderService;

@RestController
public class OrderController {

	private final CustomerOrderService customerOrderService;

	@Autowired
	public OrderController(CustomerOrderService customerOrderService) {
		this.customerOrderService = customerOrderService;
	}

	@CrossOrigin
	@PostMapping(value = "/api/orders")
	public CustomerOrderModel saveOrder(@RequestBody CustomerOrderModel order) {
		return customerOrderService.saveOrder(order);
	}

	@CrossOrigin
	@GetMapping(value = "/api/orders/{id}")
	public CustomerOrderModel getOrder(@PathVariable long id) {
		return customerOrderService.findOrder(id);
	}
}