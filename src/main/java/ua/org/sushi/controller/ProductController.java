package ua.org.sushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ua.org.sushi.dao.model.ProductModel;
import ua.org.sushi.service.ProductService;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
public class ProductController {

	private final ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GetMapping(value = "/api/products")
	public List<ProductModel> getProducts() {
		return productService.getProducts();
	}

	@GetMapping(value = "/api/products/{id}")
	public ProductModel getProduct(@PathVariable Long id) {
		return productService.getProduct(id);
	}

	@PostMapping(value = "/api/products")
	public ProductModel saveProduct(@RequestBody @Valid ProductModel model) {
		return productService.saveProduct(model);
	}
}