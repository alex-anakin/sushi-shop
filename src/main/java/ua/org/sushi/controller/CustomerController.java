package ua.org.sushi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.org.sushi.dao.model.CustomerModel;
import ua.org.sushi.service.CustomerService;

import java.util.List;

@CrossOrigin
@RestController
public class CustomerController {

	private final CustomerService customerService;

	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}


	@PostMapping(value = "/api/customers")
	public CustomerModel saveCustomer(@RequestBody CustomerModel user) {
		return customerService.saveCustomer(user);
	}

	@GetMapping(value = "/api/customers/{id}")
	public CustomerModel getCustomer(@PathVariable Long id) {
		return customerService.getCustomer(id);
	}

	@GetMapping(value = "/api/customers")
	public List<CustomerModel> getCustomers() {
		return customerService.getCustomers();
	}
}