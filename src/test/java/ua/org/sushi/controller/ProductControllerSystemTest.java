package ua.org.sushi.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.org.sushi.TestUtility;
import ua.org.sushi.dao.model.ProductModel;
import ua.org.sushi.enums.ProductType;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerSystemTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@Test
	public void shouldGetProductById() throws Exception {
		String expectedContent = "{\"id\":1,\"name\":\"Филадельфия маки\",\"price\":120.0,\"type\":\"MAIN\"," +
				"\"imageUrl\":\"http://i63.tinypic.com/25543rr.jpg\"}";

		this.mockMvc.perform(get("/api/products/1").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string(expectedContent));
	}

	@Test
	public void shouldGetProducts() throws Exception {
		String expectedFirst = "Филадельфия маки";
		String expectedLast = "Палочки";

		this.mockMvc.perform(get("/api/products").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name").value(expectedFirst))
				.andExpect(jsonPath("$[12].name").value(expectedLast));
	}

	@Test
	public void shouldSaveProduct() throws Exception {

		ProductModel product = buildProduct();
		String json = new TestUtility<>().buildJson(product);

		String expectedContent = "{\"id\":14,\"name\":\"Суши с тунцом\",\"price\":35.0,\"type\":\"MAIN\"," +
				"\"imageUrl\":\"/some/path/to/image.jpg\"}";

		this.mockMvc.perform(
				post("/api/products")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isOk())
				.andExpect(content().string(expectedContent));
	}

	@Test
	public void shouldFailWhileSavingProductWithNullField() throws Exception {
		ProductModel product = buildProduct();
		product.setPrice(null);
		String json = new TestUtility<>().buildJson(product);

		String message = "Validation failed for classes [ua.org.sushi.dao.entity.Product]";

		this.mockMvc.perform(
				post("/api/products")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isBadRequest())
				.andExpect(content().string(containsString(message)));
	}

	private ProductModel buildProduct() {
		ProductModel product = new ProductModel();
		product.setName("Суши с тунцом");
		product.setPrice(35.0d);
		product.setType(ProductType.MAIN);
		product.setImageUrl("/some/path/to/image.jpg");
		return product;
	}
}
