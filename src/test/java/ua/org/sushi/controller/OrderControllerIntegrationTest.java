package ua.org.sushi.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ua.org.sushi.TestUtility;
import ua.org.sushi.dao.model.CustomerModel;
import ua.org.sushi.dao.model.CustomerOrderModel;
import ua.org.sushi.dao.model.ShoppingCartModel;
import ua.org.sushi.service.CustomerOrderService;

import java.util.ArrayList;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerOrderService orderService;

	@Before
	public void init() {
		mockMvc = MockMvcBuilders
				.standaloneSetup(new OrderController(orderService))
				.build();
	}

	@Test
	public void shouldReturnOrderWhileSaving() throws Exception {
		CustomerOrderModel order = buildOrder();
		String json = new TestUtility<>().buildJson(order);
		order.setId(1000L);

		when(orderService.saveOrder(any(CustomerOrderModel.class))).thenReturn(order);

		String expectedJson = "{\"id\":1000," +
				"\"customer\":{\"id\":5,\"email\":\"alex@gmail.com\",\"name\":\"Alex\",\"address\":null}," +
				"\"shoppingCart\":{\"id\":726,\"orders\":[]}}";

		mockMvc.perform(
				post("/api/orders")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isOk())
				.andExpect(content().string(expectedJson));
	}

	@Test
	public void shouldReturnOrderWhenGetById() throws Exception {
		long id = 2000L;
		CustomerOrderModel order = buildOrder();
		order.setId(id);
		String json = new TestUtility<>().buildJson(order);

		when(orderService.findOrder(id)).thenReturn(order);

		String expectedJson = "{\"id\":2000," +
				"\"customer\":{\"id\":5,\"email\":\"alex@gmail.com\",\"name\":\"Alex\",\"address\":null}," +
				"\"shoppingCart\":{\"id\":726,\"orders\":[]}}";

		mockMvc.perform(
				get("/api/orders/2000")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json))
				.andExpect(status().isOk())
				.andExpect(content().string(expectedJson));
	}

	@Test
	public void shouldReturnBadRequestWhenGetOrderWithTextId() throws Exception {
		mockMvc.perform(
				get("/api/orders/any_text"))
				.andExpect(status().isBadRequest());
	}

	private CustomerOrderModel buildOrder() {
		CustomerModel customer = new CustomerModel();
		customer.setId(5L);
		customer.setName("Alex");
		customer.setEmail("alex@gmail.com");

		ShoppingCartModel shoppingCart = new ShoppingCartModel();
		shoppingCart.setId(726L);
		shoppingCart.setOrders(new ArrayList<>());

		CustomerOrderModel order = new CustomerOrderModel();
		order.setCustomer(customer);
		order.setShoppingCart(shoppingCart);
		return order;
	}
}
