package ua.org.sushi.service;

import ma.glasnost.orika.MapperFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ua.org.sushi.dao.entity.Customer;
import ua.org.sushi.dao.entity.CustomerOrder;
import ua.org.sushi.dao.model.CustomerModel;
import ua.org.sushi.dao.model.CustomerOrderModel;
import ua.org.sushi.dao.model.ShoppingCartModel;
import ua.org.sushi.dao.repository.CustomerOrderRepository;
import ua.org.sushi.dao.repository.CustomerRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerOrderServiceUnitTest {

	@Mock
	private CustomerOrderRepository customerOrderRepository;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private MapperFacade mapper;

	private CustomerOrderService service;

	@Before
	public void init() {
		service = new CustomerOrderService(customerOrderRepository, customerRepository, mapper);
	}

	@Test
	public void shouldSaveOrder() throws Exception {
		long id = 1L;
		CustomerOrderModel customerOrderModel = buildCustomerOrder();
		customerOrderModel.getCustomer().setId(id);
		CustomerOrder customerOrder = new CustomerOrder();

		when(customerRepository.findOne(id)).thenReturn(new Customer());
		when(mapper.map(customerOrderModel, CustomerOrder.class)).thenReturn(customerOrder);
		when(customerOrderRepository.save(any(CustomerOrder.class))).thenReturn(customerOrder);
		when(mapper.map(customerOrder, CustomerOrderModel.class)).thenReturn(customerOrderModel);

		service.saveOrder(customerOrderModel);

		int expectedInvocations = 1;
		verify(customerRepository, times(expectedInvocations)).findOne(id);
		verify(customerOrderRepository, times(expectedInvocations)).save(customerOrder);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowExceptionWhileSavingOrderWithoutCustomer() throws Exception {
		CustomerOrderModel customerOrder = buildCustomerOrder();
		service.saveOrder(customerOrder);
	}

	private CustomerOrderModel buildCustomerOrder() {
		CustomerOrderModel customerOrder = new CustomerOrderModel();
		CustomerModel customer = new CustomerModel();
		ShoppingCartModel shoppingCart = new ShoppingCartModel();
		customerOrder.setCustomer(customer);
		customerOrder.setShoppingCart(shoppingCart);
		return customerOrder;
	}
}
