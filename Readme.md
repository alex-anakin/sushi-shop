# sushi-shop API

Run application
1. Go to the project folder
2. Run command 'gradlew.bat bootRun' for Windows OS or './gradlew bootRun' for Linux OS
3. Open url http://localhost:8080/swagger-ui.html in browser

Order of calls
1. get products
2. save shopping cart
3. get shopping cart
4. save customer
5. save customer order
6. get customer order if needed